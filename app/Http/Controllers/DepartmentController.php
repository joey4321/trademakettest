<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\GlobalController;
use App\Department;
use App\Http\Request\departmentRequest;
class DepartmentController extends GlobalController
{

     public function __construct(){
      $this->model = new Department;
      $this->url = url('department');
      $this->hasDetails = true;
      $this->datatablecolumns = [
          [
                'label' => "persons",
                'type' => 'counter',
                'entity' => 'persons', // the method that defines the relationship in your Model
                'attribute' => 'name', // foreign key attribute that is shown to user
                'name'=>'persons'
          ]
        ];
         
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('main');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(departmentRequest $request)
    {
       $deparment = new Department;
        $deparment->name = $request->get('name');
        $deparment->phone_number =$request->get('phone_number');
        $deparment->save();
         return response()->json(['message'=>'Department Created'],201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
