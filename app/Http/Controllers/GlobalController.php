<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Datatables;
class GlobalController extends Controller
{
    public $model=null;
    public $datatablecolumns=array();
    public $datatablecolumnsdetails=array();
    public $url;
    public $hasDetails=false;
    public $action =true;
    public function Datatable(){

    	 $datatables = Datatables::of($this->model->get());
        foreach ($this->datatablecolumns as $column) {

        	if ($column['type'] == 'select') {
                $datatables = $datatables->addColumn($column['entity'], function ($user) use ($column) {
                    if (isset($user->{$column['name']})) {
                        if(isset($user->{$column['name']}[$column['attribute']])){
                            return $user->{$column['name']}[$column['attribute']];
                        }else{
                            return '';
                        }

                    } else {
                        return '';
                    }
                });
            }
            if ($column['type'] == 'select_multiple') {
                $datatables = $datatables->addColumn($column['name'], function ($user) use ($column) {
                    $string = '';
                    $flag = false;
                    foreach ($user->{$column['name']} as $key) {
                    	if($flag==false){
                    		 	$string .= $key->{$column['attribute']} . "";
                    		 	$flag=true;
                    		}else{
                    			 $string .=  ",<br/>".$key->{$column['attribute']} ;
                    		}
                       
                    }
                    return $string;
                });
            }
            if ($column['type'] == 'counter') {
                $datatables = $datatables->addColumn($column['name'], function ($user) use ($column) {
                    return $user->{$column['name']}->count();
                });
            }
        }
        if($this->action){
        $datatables= $datatables->addColumn('action', function ($user) {

                $button= '
                                            ';
                if($this->hasDetails){
                    $button.= '<button id="' . $user->id . '" class="btn btn-info view" ><i class="fa fa-binoculars"></i></button>';
                }                                
                return $button;
            });
         }
         return $datatables->make(true);
    }

    public function DatatableAttach($attach){

         $datatables = Datatables::of($this->model->get());
        foreach ($this->datatablecolumns as $column) {

            if ($column['type'] == 'select') {
                $datatables = $datatables->addColumn($column['entity'], function ($user) use ($column) {
                    if (isset($user->{$column['name']})) {
                        if(isset($user->{$column['name']}[$column['attribute']])){
                            return $user->{$column['name']}[$column['attribute']];
                        }else{
                            return '';
                        }

                    } else {
                        return '';
                    }
                });
            }
            if ($column['type'] == 'select_multiple') {
                $datatables = $datatables->addColumn($column['name'], function ($user) use ($column) {
                    $string = '';
                    $flag = false;
                    foreach ($user->{$column['name']} as $key) {
                        if($flag==false){
                                $string .= $key->{$column['attribute']} . "";
                                $flag=true;
                            }else{
                                 $string .=  ",<br/>".$key->{$column['attribute']} ;
                            }
                       
                    }
                    return $string;
                });
            }
            if ($column['type'] == 'counter') {
                $datatables = $datatables->addColumn($column['name'], function ($user) use ($column) {
                    return $user->{$column['name']}->count();
                });
            }
        }

        $datatables= $datatables->addColumn('action', function ($user) use ($attach){

                $button= '
                                             <button id="' . $user->id . '" class="btn btn-xs btn-success attach" data-list="'.$attach.'" @click="destroy"><i class="fa fa-plus"></i></button>
                                             ';
                                             
                return $button;
            });

         return $datatables->make(true);
    }

    public function DatatableDetails($relation,$id){
          
         $datatables = Datatables::of($this->model->where('id',$id)->first()->{$relation}()->get());
      
        foreach ($this->datatablecolumnsdetails as $column) {

            if ($column['type'] == 'select') {
                $datatables = $datatables->addColumn($column['entity'], function ($user) use ($column) {
                    if (isset($user->{$column['name']})) {
                        if(isset($user->{$column['name']}[$column['attribute']])){
                            return $user->{$column['name']}[$column['attribute']];
                        }else{
                            return '';
                        }

                    } else {
                        return '';
                    }
                });
            }
            if ($column['type'] == 'select_multiple') {
                $datatables = $datatables->addColumn($column['name'], function ($user) use ($column) {
                    $string = '';
                    $flag = false;
                    foreach ($user->{$column['name']} as $key) {
                        if($flag==false){
                                $string .= $key->{$column['attribute']} . "";
                                $flag=true;
                            }else{
                                 $string .=  ",<br/>".$key->{$column['attribute']} ;
                            }
                       
                    }
                    return $string;
                });
            }
            if ($column['type'] == 'counter') {
                $datatables = $datatables->addColumn($column['name'], function ($user) use ($column) {
                    return $user->{$column['name']}->count();
                });
            }
        }

        $datatables= $datatables->addColumn('action', function ($user) use($id) {

                $button= '
                                            
                                             <button id="' . $user->id . '" class="btn btn-xs btn-danger delete" data-list="'.$id.'" @click="destroy"><i class="icon-close icons"></i></button>
                                             ';

                return $button;
            });

         return $datatables->make(true);
    }

}
