<?php

namespace App\Http\Controllers;

use App\Http\Request\PersonRequest;
use Illuminate\Http\Request;
use App\Person;
class PersonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($department_id, PersonRequest $request)
    {
        $person = new Person;
        $person->name = $request->get('name');
        $person->email = $request->get('email');
        $person->address = $request->get('address');
        $person->phone_number = $request->get('phone_number');
        $person->department_id = $department_id;
        $person->save();
        return response()->json(['message'=>'Person Created'],201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $person = Person::find($id);
        if(ise_null($person)){
             return response()->json(['message'=>'Person not found'],403);
        }
        $person->delete();
         return response()->json(['message'=>'Person Deleted'],201);
    }
}
