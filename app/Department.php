<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
	// return many persons object
    public function persons(){
    	return $this->hasMany(Person::class);
    }
}
