@extends('layouts.app')
@section('breadcrumb')
 <!-- Breadcrumb -->
        

@endsection
@section('contains')
<div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-header">
                                    <i class="fa fa-align-justify"></i>  Lists
                                    <div class="card-actions btn-primary">
                                        <a href="#"  data-toggle="modal" data-target="#primaryModal">
                                           <i class="fa fa-plus"></i>
                                        </a>
                                    </div>
                                </div>
                                 <div class="card-block">
                                    <table class="table table-striped table-bordered datatable">
                                        <thead>
                                            <tr>
                                                <th>id</th>
                                                <th>Department Name</th>
                                                <th># Of Person</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                       
                                    </table>
                                   
                                        
                                    
                                </div>
                            </div>
                        </div>
                        <!--/.col-->
                    </div>
    <div class="modal fade" id="primaryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-primary" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Create Department</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-body">
                               @include('department.fields')
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary save-department"  >Save changes</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->                
                    <!--/.row-->
  <script id="person-attach-list" type="text/x-handlebars-template">
              @include('department.details',['id'=>'detail-@{{id}}','title'=>'Persons Attach of this list'])
              
    </script>
@endsection
@section('script-assets')
  <!-- Plugins and scripts required by this views -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.0.5/handlebars.min.js"></script>
    <script src="/assets/js/libs/jquery.dataTables.min.js"></script>
    <script src="/assets/js/libs/dataTables.bootstrap4.min.js"></script>
    <script src="/assets/js/libs/toastr.min.js"></script>
    <script type="text/javascript">
        var table;
        var template;
        var template_contact;
        var table_template={};
        $(document).on('click', 'button.delete', function () {
            var tr = $(this).closest('tr');
            var row = table.row(tr);
            //$('.delete').click(function () {
            //alert("I am an alert box!");
            if (confirm("Do you want to delete this Person?")) {
                var CSRF_TOKEN =window.Laravel.csrfToken;
                var id = this.id;
                $.ajax({
                    url: "{{url('/person/delete/')}}/" + this.id,
                    type: 'DELETE',
                    dataType: 'json',
                    data: {_token: CSRF_TOKEN},
                    success: function (data, status, header) {
                        row.remove();
                        tr.remove();
                    },
                    error: function (data, status, header) {
                        console.log(data);
                    }
                })
            }
        })
       
         $(document).ready(function () {
      	var progress = $('.sk-wave').hide();
        template = Handlebars.compile($("#person-attach-list").html());
      

         table= $('.datatable').DataTable({
            "sDom": "<'row mb-1'<'col-sm-6'l><'col-sm-6'f>r>t<'row'<'col-sm-6'i><'col-sm-6 center'p>>",
             processing: true,
              serverSide: true,
            renderer: 'bootstrap',
             ajax: '{{url("/department/datatable")}}',
             columns: [
                    {data: 'id', name: 'id', orderable: true, searchable: true},
                    {data: 'name', name: 'name', orderable: true, searchable: true},
                    {data: 'persons', name: 'persons', orderable: true, searchable: true},
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ],
            order: [0, 'desc'],
            // "oLanguage": {
            //   "sLengthMenu": "_MENU_ records per page"
            // }
          });
        });
        // Add event listener for opening and closing details
    $(document).on('click', 'button.view', function () {
        console.log('clicked');
        var tr = $(this).closest('tr');
        var row = table.row(tr);
        var tableId = 'detail-' + row.data().id;
        var tableIdcontact = 'detail-contact-' + row.data().id;
        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {
            // Open this row
            row.child([template(row.data())]).show();
            //row.appendChild( template_movies(row.data())).show();
            initTable(tableId, row.data());
        
            tr.addClass('shown');
            tr.next().find('td').addClass('no-padding bg-gray');
        }
    });
    // create person for department
    $(document).on('click', 'button.save-person', function () {
    		var id =this.id;
    		var progress =$('.hide_'+id);
    		console.log(progress);
    		progress.show();
    		 var CSRF_TOKEN =window.Laravel.csrfToken;
    		var fields = {
    			'name':$('.name_'+id).val(),
    			'email':$('.email_'+id).val(),
    			'phone_number':$('.phone_number_'+id).val(),
    			'address':$('.address_'+id).val(),
    			_token: CSRF_TOKEN
    		}
    		
              
                var list =this.id;
                
                $.ajax({
                    url: "{{url('/person/create')}}/" +this.id,
                    type: 'POST',
                    dataType: 'json',
                    data: fields,
                    success: function (data, status, header) {
                         toastr.success('Person Created for this department', data.message, {
                            closeButton: true,
                            progressBar: false,
                          });
                         console.log(table_template);
                         table_template['detail-' + list].ajax.reload();
                         progress.hide();
                          $("#secondaryModal .close").click()
                    },
                    error: function (data, status, header) {
                        console.log(data);
                        alert(data.responseText);
                        progress.hide()
                    }
                })
    });

     // create person for department
    $(document).on('click', 'button.save-department', function () {
    		
    		var progress =$('.department');
    		console.log(progress);
    		progress.show();
    		 var CSRF_TOKEN =window.Laravel.csrfToken;
    		var fields = {
    			'name':$('.name').val(),
    			'phone_number':$('.phone_number').val(),
    			_token: CSRF_TOKEN
    		}
    		
              
              
                
                $.ajax({
                    url: "{{url('/department/create')}}/" +this.id,
                    type: 'POST',
                    dataType: 'json',
                    data: fields,
                    success: function (data, status, header) {
                         toastr.success('Department Created ', data.message, {
                            closeButton: true,
                            progressBar: false,
                          });
                         
                         table.ajax.reload();
                         progress.hide();
                          $("#primaryModal .close").click()
                    },
                    error: function (data, status, header) {
                        console.log(data);
                        alert(data.responseText);
                        progress.hide()
                    }
                })
    });
    function initTable(tableId, data) {
        console.log(data)
       table_template[tableId] = $('#' + tableId).DataTable({
            "sDom": "<'row mb-1'<'col-sm-6'l><'col-sm-6'f>r>t<'row'<'col-sm-6'i><'col-sm-6 center'p>>",
             processing: true,
              serverSide: true,
            renderer: 'bootstrap',
             ajax: '{{url("/department/persons/")}}/'+data.id,
             columns: [
                    {data: 'id', name: 'id', orderable: true, searchable: true},
                    {data: 'name', name: 'name', orderable: true, searchable: true},
                    {data: 'email', name: 'email', orderable: true, searchable: true},
                    {data: 'address', name: 'address', orderable: true, searchable: true},
                    {data: 'phone_number', name: 'phone_number', orderable: true, searchable: true},
                    
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ],
                order: [0, 'asc'],
            // "oLanguage": {
            //   "sLengthMenu": "_MENU_ records per page"
            // }
          });

    }
    
    </script>

   
@endsection