<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Genius - Bootstrap 4 Admin Template">
    <meta name="author" content="Łukasz Holeczek">
    <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,AngularJS,Angular,Angular2,jQuery,CSS,HTML,RWD,Dashboard">
    <link rel="shortcut icon" href="img/favicon.png">
    <meta name="csrf-token" id="token" content="{{ csrf_token() }}" />
    <title>Genius - Bootstrap 4 Admin Template</title>

    <!-- Icons -->
    <link href="/assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="/assets/css/simple-line-icons.css" rel="stylesheet">

    <!-- Premium Icons -->
    <link href="/assets/css/glyphicons.css" rel="stylesheet">
    <link href="/assets/css/glyphicons-filetypes.css" rel="stylesheet">
    <link href="/assets/css/glyphicons-social.css" rel="stylesheet">

    <!-- Main styles for this application -->
    <link href="/assets/css/style.css" rel="stylesheet">
    @yield('header-assets')
    <script>
        window.Laravel = {!! json_encode([
        'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<!--
 * GenesisUI - Bootstrap 4 Admin Template
 * @version v1.8.1
 * @link https://genesisui.com
 * Copyright (c) 2017 creativeLabs Łukasz Holeczek
 * @license Commercial
 -->
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Genius - Bootstrap 4 Admin Template">
    <meta name="author" content="Łukasz Holeczek">
    <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,AngularJS,Angular,Angular2,jQuery,CSS,HTML,RWD,Dashboard">
    <link rel="shortcut icon" href="img/favicon.png">
    <meta name="csrf-token" id="token" content="{{ csrf_token() }}" />
    <title>Genius - Bootstrap 4 Admin Template</title>

    <!-- Icons -->
    <link href="/assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="/assets/css/simple-line-icons.css" rel="stylesheet">

    <!-- Premium Icons -->
    <link href="/assets/css/glyphicons.css" rel="stylesheet">
    <link href="/assets/css/glyphicons-filetypes.css" rel="stylesheet">
    <link href="/assets/css/glyphicons-social.css" rel="stylesheet">

    <!-- Main styles for this application -->
    <link href="/assets/css/style.css" rel="stylesheet">
    @yield('header-assets')
    <script>
        window.Laravel = {!! json_encode([
        'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>

<!-- BODY options, add following classes to body to change options

// Header options
1. '.header-fixed'					- Fixed Header

// Sidebar options
1. '.sidebar-fixed'					- Fixed Sidebar
2. '.sidebar-hidden'				- Hidden Sidebar
3. '.sidebar-off-canvas'		- Off Canvas Sidebar
4. '.sidebar-compact'				- Compact Sidebar Navigation (Only icons)

// Aside options
1. '.aside-menu-fixed'			- Fixed Aside Menu
2. '.aside-menu-hidden'			- Hidden Aside Menu
3. '.aside-menu-off-canvas'	- Off Canvas Aside Menu

// Footer options
1. 'footer-fixed'						- Fixed footer

-->

<body class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden" @yield('vue-app')>
   

    <div class="app-body">
        <div class="sidebar">
        </div>

        <!-- Main content -->
        <main class="main" id="employeer" @yield('vue-props')>

           @yield('breadcrumb')

            <div class="container-fluid">



                @yield('contains')


            </div>
            <!-- /.conainer-fluid -->
        </main>
    </div>

    
    <!-- Bootstrap and necessary plugins -->
    <script src="/assets/js/libs/jquery.min.js"></script>
    <script src="/assets/js/libs/tether.min.js"></script>
    <script src="/assets/js/libs/bootstrap.min.js"></script>
    <script src="/assets/js/libs/pace.min.js"></script>


    <!-- Plugins and scripts required by all views -->
    <script src="/assets/js/libs/Chart.min.js"></script>


    <!-- GenesisUI main scripts -->

    <script src="/assets/js/app.js"></script>
    @yield('script-assets')

</body>


</html>