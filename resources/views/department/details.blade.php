<div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-header">
                                    <i class="fa fa-align-justify"></i> {{$title}}
                                    <div class="card-actions btn-primary">
                                       <a href="#"  data-toggle="modal" data-target="#secondaryModal">
                                           <i class="fa fa-plus"></i>
                                        </a>   
                                    </div>
                                </div>
                                 <div class="card-block">
                                    <table class="table table-striped table-bordered datatable"  id='{{$id}}'>
                                        <thead>
                                            <tr>
                                                 <th>id</th>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Address</th>
                                                <th>Phone Number</th>
                                                
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                       
                                    </table>
                                   
                                        
                                    
                                </div>
                            </div>
                        </div>
                        <!--/.col-->
</div>

<div class="modal fade" id="secondaryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-info " role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Create Person</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-body">
                               @include('department.person-fields')
                            </div>
                            
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary"  >Save </button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal --> 
