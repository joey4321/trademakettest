<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','DepartmentController@index');

Route::get('/department/datatable','DepartmentController@Datatable');
Route::get('/department/{relation}/{id}', 'DepartmentController@DatatableDetails');
Route::post('/person/create/{id}', 'PersonController@store');
Route::post('/department/create', 'DepartmentController@store');