<?php

use Illuminate\Database\Seeder;
use App\Department;
use App\Person;
class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$name =['joey','jade','john','joseph','james','blessy','ann','riza','teres','sam'];
    	$email = ['@yahoo.com','@gmail.com','@sample.com','@test.com'];
    	$address = ['253 M.L. Quezon Street Santo Niño Tukuran 7019 Zamboanga del Sur','7114 Kundiman Street, Sampaloc 1008 Manila Philippines','P.O. Box 1201, Manila Central Post Office 1050 Manila','P.O. Box 1000, Gasan Post Office Gasan 4905 Marinduque Philippines'];
    	$phone_number= ['202-555-0154','202-555-0163','202-555-0100','202-555-0141','202-555-0136','202-555-0182'];
    	$departments = ['Commissioner for Public Appointments for Northern Ireland',
					'Inquiry into Historical Institutional Abuse',
					'North South Ministerial Council Joint Secretariat (North)',
					'Non-Departmental Public Bodies:[3]',
					'Commissioner for Children and Young People for Northern Ireland (only involved with appointment of commissioner)',
					'Commissioner for Older People for Northern Ireland (only involved with appointment of the commissioner)',
					'Community Relations Council for Northern Ireland',
					'Commission for Victims and Survivors for Northern Ireland',
					'Equality Commission for Northern Ireland',
					'Maze/Long Kesh Development Corporation',
					'Northern Ireland Judicial Appointments Commission',
					'Strategic Investment Board',
					'Victims and Survivors Service'];
		 $departmentcount = rand(10,1000);
		 for($i = 0; $i <= $departmentcount; $i++){
		 		$deparment = new Department;
		 		$deparment->name = $departments[rand(0,12)];
		 		$deparment->phone_number = $phone_number[rand(0,5)];
		 		$deparment->save();
		 		$person_count = rand(5,100);
		 		for($p = 0; $p  <= $person_count; $p++){
		 			$person = new Person;
		 			$person->name = $name[rand(0,9)];
           			$person->email = $person->name.$email[rand(0,3)];
            		$person->address = $address[rand(0,3)];
            		$person->phone_number = $phone_number[rand(0,5)];
            		$person->department_id = $deparment->id;
            		$person->save();
		 		}
		 }
          
    }
}
